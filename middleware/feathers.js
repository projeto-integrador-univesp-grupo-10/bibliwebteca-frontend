export default function ({ store, redirect, route }) {
  const { auth } = store.state
  if (auth.privatePages.includes(route.name) && auth.payload && auth.user && auth.user.isVerified === 0) {
    return redirect('/verify')
  } else if (auth.privatePages.includes(route.name) && !auth.payload) {
    return redirect('/login')
  } else if (auth.publicPages.includes(route.name) && auth.payload) {
    return redirect('/grades')
  }
}
