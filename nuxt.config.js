import colors from 'vuetify/es5/util/colors'

export default {
  // Global page headers: https://go.nuxtjs.dev/config-head
  vue: {
    config: {
      productionTip: process.env.NODE_ENV === 'production',
      devtools: process.env.NODE_ENV !== 'production'
    }
  },
  head: {
    titleTemplate: '%s - ' + process.env.npm_package_description,
    title: process.env.npm_package_name,
    htmlAttrs: {
      lang: 'pt-br'
    },
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: process.env.npm_package_description },
      { name: 'format-detection', content: 'telephone=no' }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }
    ]
  },

  // Global CSS: https://go.nuxtjs.dev/config-css
  css: [
    { src: '~/assets/bootstrap.min.css', lang: 'css' },
    { src: '~/assets/animate.min.css', lang: 'css' },
    { src: '~/assets/flash.css', lang: 'css' }
  ],

  // Plugins to run before rendering page: https://go.nuxtjs.dev/config-plugins
  plugins: [
    { src: '~/plugins/feathers.js', ssr: true },
    { src: '~/plugins/feathers-vuex.js', ssr: true },
    { src: '~/plugins/flash.js', ssr: true },
    { src: '~/plugins/vue-apexcharts.js', ssr: false }
  ],

  // Auto import components: https://go.nuxtjs.dev/config-components
  components: true,

  // Modules for dev and build (recommended): https://go.nuxtjs.dev/config-modules
  buildModules: [
    // https://go.nuxtjs.dev/eslint
    '@nuxtjs/eslint-module',
    // https://go.nuxtjs.dev/vuetify
    '@nuxtjs/vuetify'
  ],

  // Modules: https://go.nuxtjs.dev/config-modules
  modules: [
    'nuxt-client-init-module',
    '@nuxtjs/axios',
    ['vue-wait/nuxt', { useVuex: true } ],
    '@nuxtjs/dayjs',
    'nuxt-d3-module'
  ],

  // Axios module configuration: https://go.nuxtjs.dev/config-axios
  axios: {
    baseURL: process.env.NUXT_API
  },

  // Vuetify module configuration: https://go.nuxtjs.dev/config-vuetify
  vuetify: {
    customVariables: ['~/assets/variables.scss'],
    theme: {
      dark: true,
      light: false,
      themes: {
        dark: {
          primary: colors.blue.darken2,
          accent: colors.grey.darken3,
          secondary: colors.amber.darken3,
          info: colors.teal.lighten1,
          warning: colors.amber.base,
          error: colors.deepOrange.accent4,
          success: colors.green.accent3
        },
        light: {
          primary: "#22daff",
          accent: "#ff6b99",
          secondary: "#26ff8c",
          success: "#a5d64c",
          info: "#ff53d0",
          warning: "#ff8e00",
          error: "#ff5252"
        }
      }
    }
  },

  // Build Configuration: https://go.nuxtjs.dev/config-build
  build: {
    extend(config, ctx) {
      config.resolve.alias.vue = 'vue/dist/vue.common'
      if (ctx.isDev) {
        config.devtool = ctx.isClient ? 'source-map' : 'inline-source-map'
      }
      if (ctx.dev && ctx.isClient) {
        config.module.rules.push({
          enforce: 'pre',
          test: /\.(js|vue)$/,
          loader: 'eslint-loader',
          exclude: /(node_modules)/
        })
      };
    },
    extractCSS: true,
    transpile: [
      'feathers-vuex',
      './middleware/feathers.js',
      './plugins/feathers.js',
      './plugins/feathers-vuex.js',
      './plugins/flash.js',
      './plugins/vue-apexcharts.js',
      './store/index.js',
      './store/services/user.js',
      './store/services/user.hooks.js',
      './store/services/grade.js',
      './store/services/grade.hooks.js',
    ]
  },
  env: {
    API_URL: 'https://bibliwebteca-api.herokuapp.com'
  },
  router: {
    middleware: [
      'feathers'
    ]
  },
  dayjs: {
    locales: ['pt'],
    defaultLocale: 'pt',
    defaultTimeZone: 'America/Sao_Paulo',
    plugins: [
      'utc', // import 'dayjs/plugin/utc'
      'timezone' // import 'dayjs/plugin/timezone'
    ] // Your Day.js plugin
  },
  vendor: [
    'vue-apexcharts'
  ]
}
