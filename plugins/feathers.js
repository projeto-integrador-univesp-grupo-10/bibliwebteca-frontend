import dayjs from 'dayjs'
import utc from 'dayjs/plugin/utc'
import timezone from 'dayjs/plugin/timezone'
// import io from 'socket.io-client'
import axios from 'axios'
import { CookieStorage } from 'cookie-storage'
import { iff, discard } from 'feathers-hooks-common'
import feathers from '@feathersjs/feathers'
import auth, { MemoryStorage } from '@feathersjs/authentication-client'
// import socketio from '@feathersjs/socketio-client'
import rest from '@feathersjs/rest-client'
import feathersVuex, { initAuth, hydrateApi } from 'feathers-vuex'

dayjs.extend(utc)
dayjs.extend(timezone)
dayjs.tz.setDefault('America/Sao_Paulo')

const __storageOpt__ = {
  expires: new Date(dayjs().add(7, 'days').utc(true).format()),
  secure: true,
  sameSite: 'Lax'
}

const __schema__ = {
  header: 'Authorization', // the default authorization header for REST
  path: '/authentication', // the server-side authentication service path
  jwtStrategy: 'jwt', // the name of the JWT authentication strategy
  entity: 'user', // the entity you are authenticating (ie. a users)
  service: 'users', // the service to look up the entity
  cookie: 'feathers-jwt', // the name of the cookie to parse the JWT from when cookies are enabled server side
  // Passing a WebStorage-compatible object to enable automatic storage on the client.
  storage: process.server ? new MemoryStorage() : new CookieStorage(__storageOpt__)
}

const __auth__ = auth(__schema__)
const __transport__ = rest(process.env.API_URL).axios(axios)

const client = feathers()
  .configure(__transport__)
  .configure(__auth__)
  .hooks({
    before: {
      all: [
        iff(
          context => ['create', 'update', 'patch'].includes(context.method),
          discard('__id', '__isTemp')
        )
      ]
    }
  })

export default client

const { makeServicePlugin, makeAuthPlugin, BaseModel, models, FeathersVuex } = feathersVuex(
  client,
  {
    serverAlias: 'api', // optional for working with multiple APIs (this is the default value)
    idField: 'id', // Must match the id field in your database table/collection
    whitelist: ['$regex', '$options'],
    enableEvents: process.client // Prevent memory leak
  }
)

export { makeAuthPlugin, makeServicePlugin, BaseModel, models, FeathersVuex, initAuth, hydrateApi }
