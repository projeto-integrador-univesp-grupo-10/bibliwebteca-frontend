import Vue from 'vue'
import Snotify, { SnotifyPosition } from 'vue-snotify'

const options = {
  toast: {
    position: SnotifyPosition.rightTop
  },
  timeout: 5000,
  pauseOnHover: true,
  closeOnClick: true
}

Vue.use(Snotify, options)
