export default {
  forms: [
    {
      value: '',
      label: 'Email',
      type: 'text',
      counter: '32',
      rules: [
        v => !!v || 'Email é obrigatório',
        (v) => {
          const regex = /^\w+([.-]?\w+)*@\w+([.-]?\w+)*(\.\w{2,3})+$/
          return regex.test(v) || 'Email tem que ter um formato válido'
        }
      ]
    },
    {
      value: '',
      label: 'Senha',
      type: 'password',
      counter: '32',
      rules: [
        v => !!v || 'Senha é obrigatória',
        v => v.length >= 6 || 'Senha deve ter mais do que 6 caracteres'
      ]
    }
  ]
}
