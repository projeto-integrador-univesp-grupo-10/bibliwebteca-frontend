function InvalidPasswordError (messages) {
  this.name = 'InvalidPassowrd'
  this.code = 500
  this.message = 'Password invalid'
  this.errors = messages
}

function InvalidFieldError (messages) {
  this.name = 'InvalidField'
  this.code = 500
  this.message = 'Field invalid'
  this.errors = messages
}

InvalidPasswordError.prototype = Error.prototype
InvalidFieldError.prototype = Error.prototype

export {
  InvalidPasswordError,
  InvalidFieldError
}
