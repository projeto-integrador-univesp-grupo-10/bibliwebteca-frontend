export default function (error) {
  const _title = `Error ${error.code}:`
  const message = [
    `${error.name}:`,
    error.message
  ]
  for (const i in error.errors) {
    const e = error.errors[i]
    message.push(`${e.type} on ${e.path}`)
    message.push(`${e.message}`)
  }
  return {
    title: _title,
    message: message.join('\n')
  }
}
