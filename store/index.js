import Vue from 'vue'
import Vuex, { Store } from 'vuex'
import users from './services/users.service'
import auth from './services/auth.service'
import books from './services/books.service'
import { initAuth, hydrateApi, models } from '~/plugins/feathers'

if (!Vue.Vuex) {
  Vue.use(Vuex)
}

export default function () {
  return new Store({
    state: {
      edit: -1
    },
    actions: {
      nuxtServerInit ({ commit, dispatch }, { req }) {
        return initAuth({ commit, dispatch, req, moduleName: 'auth', cookieName: 'feathers-jwt' })
      },
      nuxtClientInit ({ state, commit, dispatch }) {
        hydrateApi({ api: models.api })
        if (state.auth.accessToken) {
          dispatch('auth/onInitAuth', state.auth.payload)
        }
      },
      'save/edit': ({ commit, state }, data) => {
        state.edit = data
      }
    },
    plugins: [
      users,
      auth,
      books
    ]
  })
}
