import { makeAuthPlugin } from '~/plugins/feathers'

export default makeAuthPlugin({
  userService: 'users',
  state: {
    specialPages: [],
    publicPages: [
      'login',
      'signup'
    ],
    privatePages: [
      'books'
    ],
    entityIdField: 'id',
    responseEntityField: 'user'
  },
  actions: {
    async onInitAuth ({ state, dispatch }, payload) {
      if (payload) {
        await dispatch('authenticate', { strategy: 'jwt', accessToken: state.accessToken })
      }
    }
  }
})