import { BaseModel } from '~/plugins/feathers'

class BooksModel extends BaseModel {
  static modelName = 'Books'

  static instanceDefaults = function () {
    return {
      title: '',
      author: '',
      publishing_company: '',
      year: 0,
      isbn: '',
      edition: '',
      pages: 0,
      subject: '',
      withdrawn_date: '',
      return_date: '',
      reservation_date: ''
    }
  }
}
export { BooksModel }
