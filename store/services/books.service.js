import { BooksModel } from './books.model'
import hooks from './books.hooks'
import client, { makeServicePlugin } from '~/plugins/feathers'

const SEVICE_PATH = 'books'

const books = makeServicePlugin({
  Model: BooksModel,
  service: client.service(SEVICE_PATH),
  SEVICE_PATH
})

client.service(SEVICE_PATH).hooks(hooks)

export default books
