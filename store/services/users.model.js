import { BaseModel } from '~/plugins/feathers'

class UsersModel extends BaseModel {
  static modelName = 'User'

  static instanceDefaults = function () {
    return {
      email: '',
      password: ''
    }
  }
}
export { UsersModel }
