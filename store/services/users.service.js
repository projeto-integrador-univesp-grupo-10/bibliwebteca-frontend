import { UsersModel } from './users.model'
import hooks from './users.hooks'
import client, { makeServicePlugin } from '~/plugins/feathers'

const SEVICE_PATH = 'users'

const user = makeServicePlugin({
  Model: UsersModel,
  service: client.service(SEVICE_PATH),
  SEVICE_PATH
})

client.service(SEVICE_PATH).hooks(hooks)

export default user
