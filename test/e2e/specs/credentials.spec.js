const { join } = require('path')
const chai = require('chai')
const { faker } = require('@faker-js/faker')
const rs = require('rocket-store')

rs.options({
  data_storage_area: join(__dirname, '..'),
  data_format: rs._FORMAT_JSON
})

const USER = {
  email: faker.internet.email(),
  password: faker.internet.password(),
}

describe('Bibliwebteca signup/login/logout pages', () => {

  it('should signup a new user', async () => {
    browser.url('/signup')

    // check if elements appears properly
    const $cadastrese = await $('h1=Cadastre-se')
    const $emailLabel = await browser.$('label=Email')
    const $passwordLabel = await browser.$('label=Senha')

    expect($cadastrese).toBeDisplayed()
    expect($emailLabel).toBeDisplayed()
    expect($passwordLabel).toBeDisplayed()

    // Get inputs
    const $emailInput = await $emailLabel.$(function () {
      return this.nextSibling
    })

    const $passwordInput = await $passwordLabel.$(function () {
      return this.nextSibling
    })

    const $cadastrarButton = await browser.$('span=Cadastrar').parentElement()

    // check if inputs appears
    expect($emailInput).toBeDisplayed()
    expect($passwordInput).toBeDisplayed()
    expect($cadastrarButton).toBeDisplayed()

    // elements should be with empty string
    expect($emailInput).toHaveValue('')
    expect($passwordInput).toHaveValue('')

    // put focus on element, and then type email
    await $emailInput.doubleClick()
    await browser.keys('Delete')
    await $emailInput.setValue(USER.email)
    expect($emailInput).toHaveValue(USER.email)

    // put focus on element, and then type password
    await $passwordInput.doubleClick()
    await browser.keys('Delete')
    await $passwordInput.setValue(USER.password)
    expect($passwordInput).toHaveValue(USER.password)

    // now signup
    $cadastrarButton.click()

    // the page will change
    const $notification = await browser.$('.snotifyToast__body')
    const $loginButton = await browser.$('span=Login').parentElement()

    await $notification.waitForDisplayed()
    expect($notification).toHaveText(`Usuário criado ${USER.email} criado`)
    expect($loginButton).toBeDisplayed()
  })

  it('should login the new created user', async () => {
    browser.url('/login')

    // check if elements appears properly
    const $logar = await $('h1=Login')
    const $emailLabel = await browser.$('label=Email')
    const $passwordLabel = await browser.$('label=Senha')

    expect($logar).toBeDisplayed()
    expect($emailLabel).toBeDisplayed()
    expect($passwordLabel).toBeDisplayed()

    // Get inputs
    const $emailInput = await $emailLabel.$(function () {
      return this.nextSibling
    })

    const $passwordInput = await $passwordLabel.$(function () {
      return this.nextSibling
    })

    const $loginButton = await browser.$('span=Login').parentElement()

    // elements should be with empty string
    expect($emailInput).toHaveValue('')
    expect($passwordInput).toHaveValue('')

    // put focus on element, and then type email
    await $emailInput.doubleClick()
    await browser.keys('Delete')
    await $emailInput.setValue(USER.email)
    expect($emailInput).toHaveValue(USER.email)

    // put focus on element, and then type password
    await $passwordInput.doubleClick()
    await browser.keys('Delete')
    await $passwordInput.setValue(USER.password)
    expect($passwordInput).toHaveValue(USER.password)

    // now signup
    $loginButton.click()

    // the page will change
    const $notification = await browser.$('.snotifyToast__body')
    await $notification.waitForDisplayed()
    expect($notification).toHaveText(`Bem vindo ${USER.email}`)

    const $h1UserEmail = await browser.$(`h1=${USER.email}`)
    expect($h1UserEmail).toBeDisplayed()

    const cookies = await browser.getCookies()

    chai.expect(cookies[0]).to.have.property('name')
    chai.expect(cookies[0]).to.have.property('value')
    chai.expect(cookies[0]).to.have.property('path')
    chai.expect(cookies[0]).to.have.property('domain')
    chai.expect(cookies[0]).to.have.property('secure')
    chai.expect(cookies[0]).to.have.property('httpOnly')
    chai.expect(cookies[0]).to.have.property('expiry')
    chai.expect(cookies[0]).to.have.property('sameSite')

    await rs.post('db', 'cookies', cookies)
  })

  it('should logout the new created user and clean cookies', async () => {
    const cookiesDB = await rs.get('db', 'cookies');
    let cookies = cookiesDB.result[0]
    browser.setCookies(cookies)
    browser.url('/')

    const $logout = await browser.$('#logout')
    const $notification = await browser.$('.snotifyToast__body')
    const $home = await browser.$('#home')

    expect($logout).toBeDisplayed()

    await $logout.click()

    await $notification.waitForDisplayed()
    expect($notification).toHaveText(`Sucessfully logged out`)

    await $home.waitForDisplayed()

    await browser.waitUntil(
        async () => (await browser.getCookies()).length === 0,
        { timeout: 30000 }
    )

    cookies = await browser.getCookies()
    chai.expect(cookies.length).to.be.equal(0)

  })

})
