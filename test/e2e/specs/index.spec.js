const chai = require('chai')

function menu (options) {
  const translateX = options.closed ? 'translateX(-100%)' : 'translateX(0%)'
  const expanded = options.expanded ? '256px' : '56px'
  return `height: 100vh; top: 0px; transform: ${translateX}; width: ${expanded}; max-height: calc(100% - 36px);`
}

describe('Bibliwebteca index page', () => {
  it('should have correct title', () => {
    browser.url('/')
    expect(browser).toHaveTitle('Bibliwebteca - webapp para empréstimo de livros')
  })

  it('should have correct header', async () => {
    browser.url('/')
    const $header = await browser.$('div=Bibliwebteca')
    const text = await $header.getText()
    chai.expect(text).to.equal('Bibliwebteca')
  })

  it('should close/open menu', async () => {
    browser.url('/')

    const $hamburguer = await browser.$('#hamburguer')
    const $menu = await browser.$('nav')

    $hamburguer.click()

    // before click
    //let styl = await $menu.getAttribute('style')
    expect($menu).toBeDisplayed()
    //chai.expect(styl).to.equal(menu({ closed: true, expanded: true }))

    // click close
    $hamburguer.click()
    //styl = await $menu.getAttribute('style')

    expect($menu).not.toBeDisplayed()
    //chai.expect(styl).to.equal(menu({ closed: false, expanded: true }))

    // click open
    $hamburguer.click()
    expect($menu).toBeDisplayed()
    //styl = await $menu.getAttribute('style')
    //chai.expect(styl).to.equal(menu({ closed: true, expanded: true }))
  })

  it('should click \'Signup\' and goes to signup page', async () => {
    browser.url('/')

    const $hamburguer = await browser.$('#hamburguer')
    const $signup = await browser.$('#signup')
    let $email = await browser.$('label=Email')
    let $password = await browser.$('label=Senha')

    expect($signup).toBeDisplayed()
    expect($email).not.toBeDisplayed()
    expect($password).not.toBeDisplayed()

    $signup.click()

    const $cadastrese = await $('h1=Cadastre-se')
    $email = await browser.$('label=Email')
    $password = await browser.$('label=Senha')
    expect($cadastrese).toBeDisplayed()
    expect($email).toBeDisplayed()
    expect($password).toBeDisplayed()
  })

  it('should click \'Login\' and goes to login page', async () => {
    browser.url('/')

    const $hamburguer = await browser.$('#hamburguer')
    const $signup = await browser.$('#login')
    let $email = await browser.$('label=Email')
    let $password = await browser.$('label=Senha')

    expect($signup).toBeDisplayed()
    expect($email).not.toBeDisplayed()
    expect($password).not.toBeDisplayed()

    $signup.click()

    const $cadastrese = await $('h1=Login')
    $email = await browser.$('label=Email')
    $password = await browser.$('label=Senha')
    expect($cadastrese).toBeDisplayed()
    expect($email).toBeDisplayed()
    expect($password).toBeDisplayed()
  })
})
